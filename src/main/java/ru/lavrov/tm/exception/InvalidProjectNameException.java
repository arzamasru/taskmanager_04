package ru.lavrov.tm.exception;

public class InvalidProjectNameException extends RuntimeException{
    private static final String message = "project name is empty or null!";
    public InvalidProjectNameException() {
        super(message);
    }
    public InvalidProjectNameException(String message) {
        super(message);
    }
}
