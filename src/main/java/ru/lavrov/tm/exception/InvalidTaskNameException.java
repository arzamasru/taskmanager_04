package ru.lavrov.tm.exception;

public class InvalidTaskNameException extends RuntimeException{
    private static final String message = "task name is empty or null!";
    public InvalidTaskNameException() {
        super(message);
    }
    public InvalidTaskNameException(String message) {
        super(message);
    }
}
